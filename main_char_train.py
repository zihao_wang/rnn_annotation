# -*- coding: utf-8 -*-
#pylint: skip-file
import os
import sys
import time
import numpy as np
import theano
import theano.tensor as T
from utils_pg import *
from rnn import *
import data
import cPickle as pickle
from gensim.models import Word2Vec

def get_second_largest(vector, idx_largest):
    if len(vector) < 2:
        return None, None

    idx_now = 1
    idx_second = 0 if idx_largest != 0 else 1
    val_second = vector[0] if idx_largest != 0 else vector[1]
    if len(vector) == 2:
        return idx_second, val_second

    while idx_now < len(vector):
        if idx_now == idx_largest:
            idx_now += 1
            continue
        if vector[idx_now] > val_second:
            idx_second = idx_now
            val_second = vector[idx_now]
        idx_now += 1

    return idx_second, val_second

def count_tag(tags, idx_now, width):
    num_tag1 = 0
    num_tag2 = 0
    i = idx_now - width
    while i <= idx_now + width:
        if tags[i] == "1":
            num_tag1 += 1
        elif tags[i] == "2":
            num_tag2 += 1
        i += 1

    return num_tag1, num_tag2

def binary_fill_vacancy(tags):
    width = 2
    i = width
    while i < len(tags) - width:
        if tags[i] != "1":
            num_tag1 = count_tag(tags, i, width)[0]
            if num_tag1 == 2 * width:
                tags[i] = "1"
                i += width
        i += 1

def nyt_predict(model, testing_data, output_size):
    with open("./nyt_prediction", "a") as f_dst:
        idx_data = 0
        for each_batch in testing_data:
            if len(each_batch.num_word) == 0:
                continue
            num_words = each_batch.num_word[0]
            activation = model.predict(each_batch.x, each_batch.mask, each_batch.local_batch_size)[0]
            idx_largest = np.argmax(activation, axis = 1)
            val_largest = np.max(activation, axis = 1)
            idx_second = np.zeros(num_words)
            val_second = np.zeros(num_words)
            idx_word = 0
            while idx_word < num_words:
                idx_second[idx_word], val_second[idx_word] = get_second_largest(activation[idx_word, :], idx_largest[idx_word])
                idx_word += 1
    
            #有label的词>70%
            '''
            LABEL_PERCENTAGE = 0.7
            num_labels = np.zeros(output_size)
            for e in idx_largest:
                num_labels[e] += 1
            if num_labels[0] > int(num_words * LABEL_PERCENTAGE):
                continue
            '''
    
            idx_word = 0
            LOWER_BOUND = 0.4
            PROB_DIFF = 0.01
            tag = []
            while idx_word < num_words:
                if val_largest[idx_word] > LOWER_BOUND and val_largest[idx_word] - val_second[idx_word] > PROB_DIFF:
                    tag.append(str(idx_largest[idx_word]))
                else:
                    tag.append("0")
                idx_word += 1
            words = each_batch.sentence[0].split()
            for i in xrange(len(tag)):
                f_dst.write(words[i] + " " + tag[i] + "\n")
            f_dst.write("\n")
            idx_data += 1

def testing(model, testing_data, output_size):
    num_x = 0.0
    acc = 0.0
    conf_mat = np.zeros((output_size, output_size))
    prec = np.zeros((output_size))
    recall = np.zeros((output_size))
    f1 = np.zeros((output_size))
    for idx in xrange(len(testing_data) - 1):
        each_batch = testing_data[idx]
        label = np.argmax(each_batch.y, axis = 1)
        #activation, a0, a1 = model.predict(each_batch.x, each_batch.mask, each_batch.local_batch_size)
        activation = model.predict(each_batch.x, each_batch.mask, each_batch.local_batch_size)[0]
        #print activation
        #print a0
        #print a1
        #sys.exit(0)
        p_label = np.argmax(activation, axis = 1)
    
        '''
        if idx < 10:
            print label[0 : each_batch.num_word[0]].transpose()
            print p_label[0 : each_batch.num_word[0]].transpose()
            print "------------------------------------"
        '''
        for c in xrange(each_batch.num_word[0]):
            num_x += 1
            if label[c] == p_label[c]:
                acc += 1
            conf_mat[p_label[c], label[c]] += 1

    #print conf_mat
    #print "Accuracy = " + str(acc / num_x)
    for idx in xrange(output_size):
        row_sum = conf_mat[:, idx].sum()
        col_sum = conf_mat[idx, :].sum()
        prec[idx] = conf_mat[idx, idx] / row_sum if row_sum != 0.0 else 0.0 
        recall[idx] = conf_mat[idx, idx] / col_sum if col_sum != 0.0 else 0.0
        f1[idx] = 2 * prec[idx] * recall[idx] / (prec[idx] + recall[idx])
        print "label " + str(idx) + " precision: " + str(prec[idx]) + "\trecall: " + str(recall[idx]) + "\tf1: " + str(f1[idx])
    return prec, recall, f1


def tac_predict(model, output_size, using_w2v):
    num_label = np.zeros(output_size)
    #idx_data = 0
    #idx_topic = 0

    num_0 = 0
    num_1 = 0
    src_dir = "./tac/rnn_input/"
    for ld in os.listdir(src_dir):
        if ld == "i2w" or ld == "w2i":
            continue
        src_path = os.path.join(src_dir, ld)
        with open("./tac/rnn_prediction/" + ld, "w") as f_dst:
            print "processing", ld
            testing_data = data.read_data(src_path, trans, using_w2v, 1, False, True, False, output_size)[0]
            print "testing data size:", len(testing_data)

            for each_batch in testing_data:
                if len(each_batch.num_word) == 0:
                    continue
                num_words = each_batch.num_word[0]
                activation = model.predict(each_batch.x, each_batch.mask, each_batch.local_batch_size)[0]
                idx_largest = np.argmax(activation, axis = 1)
                val_largest = np.max(activation, axis = 1)
                idx_second = np.zeros(num_words)
                val_second = np.zeros(num_words)
                idx_word = 0
                while idx_word < num_words:
                    idx_second[idx_word], val_second[idx_word] = get_second_largest(activation[idx_word, :], idx_largest[idx_word])
                    idx_word += 1

                idx_word = 0
                LOWER_BOUND = 0.395
                PROB_DIFF = 0.0
                tag = []
    
                while idx_word < num_words:
                    if idx_largest[idx_word] == 1 or (idx_largest[idx_word] == 0 and val_second[idx_word] > LOWER_BOUND):
                        tag.append("1")
                        #tag.append(str(idx_largest[idx_word]))
                    else:
                        tag.append("0")
                    idx_word += 1
                words = each_batch.sentence[0].split()

                binary_fill_vacancy(tag)
                num_tags = tag.count("1")
                if num_tags > 3 and float(num_tags) / len(words) > 0.8:
                    for i in xrange(len(tag)):
                        if tag[i] == "0":
                            tag[i] = "1"
            
                if num_tags <= 3:
                    for i in xrange(len(tag)):
                        tag[i] = "0"

                i = 0
                while i < len(tag):
                    if tag[i] == "1":
                        idx_start = i
                        num_words = 1
                        idx_end = i + 1
                        while idx_end < len(tag):
                            if tag[idx_end] == "0":
                                break
                            idx_end += 1
                            num_words += 1
                        if num_words <= 4:
                            while idx_start < idx_end:
                                tag[idx_start] = "0"
                                idx_start += 1
                        i = idx_end
                    else:
                        i += 1

                num_0 += tag.count("0")
                num_1 += tag.count("1")

                for i in xrange(len(tag)):
                    f_dst.write(words[i] + "\\" + tag[i] + "\\" + str(val_largest[i]) + "\n")
                    num_label[idx_largest[i]] += 1
                f_dst.write("\n")
                '''
                idx_data += 1
                if idx_data >= num_info[idx_topic]:
                    idx_data = 0
                    idx_topic += 1
                '''
        print "label 0:", num_0, "label 1:", num_1

use_gpu(1)

e = 0.01
lr = 1
drop_rate = 0.
batch_size = 20
hidden_size = [100, 100]
num_model = 1
num_training_file = 20
#num_testing_file = 6
predict = 1
if predict:
    need_load_model = 1
    training_model = 0
    predict_model = 1
else:
    need_load_model = 0
    training_model = 1
    predict_model = 0
using_w2v = False
# try: gru, lstm
cell = "gru"
# try: sgd, momentum, rmsprop, adagrad, adadelta, adam
optimizer = "adadelta" 

trans_name = "word2vec" if using_w2v else "one hot"
model_name = "w2v.model" if using_w2v else "one_hot.model"
print "loading " + trans_name + " module..."
trans = Word2Vec.load_word2vec_format("/misc/projdata12/info_fil/zhwang/workspace/rnn_text_annotation/raw_data/w2v.bin", binary=True) if using_w2v else pickle.load(open("./w2i", "r"))

start = time.time()
g_error = 9999.9999
max_epoch = 2
for idx_model in xrange(num_model):
    input_size, output_size = data.read_size("./training_data/training_data_0", using_w2v, trans)
    print "#input size:", input_size, "#label:", output_size
    result = np.zeros((output_size, 3))
    #testing_data = data.read_data("/gds/zhwang/zhwang/data/cuhk/data/testing_data_" + str(idx_model), trans, using_w2v, 1)[0]
    print "compiling model " + str(idx_model + 1) 
    model = BdRNN(input_size, output_size, hidden_size, cell, optimizer, drop_rate)
    if need_load_model:
        print "load model"
        model = load_model("./model/" + model_name, model)

    if training_model:
        print "training model " + str(idx_model + 1) 
        print "number of models in cross validation: " + str(num_model)
        for epoch in xrange(max_epoch):
            print "epoch = " + str(epoch)
            total_error = 0.0
            total_num_training_data = 0
            in_start = time.time()
            for idx_data_file in xrange(num_training_file):
                error = 0.0
                training_data, num_training_data = data.read_data("./training_data/training_data_" + str(idx_model), trans, using_w2v, batch_size, True, False, True)
                print "training model with dataset", idx_data_file + 1
                for each_batch in training_data:
                    cost = model.train(each_batch.x, each_batch.mask, each_batch.y, lr, each_batch.local_batch_size)[0]
                    error += cost
                total_error += error
                total_num_training_data += num_training_data
                error /= num_training_data;
                print "loss:", error
                print "save model..."
                save_model("./model/" + model_name, model)
        
            #prec, recall, f1 = testing(model, testing_data, output_size)
            total_error /= total_num_training_data;
            in_time = time.time() - in_start
            print "total average loss:", total_error, "time:", in_time
    else:
        print "skip training model"

    if predict_model:
        tac_predict(model, output_size, using_w2v)
    '''
    result[:, 0] += prec
    result[:, 1] += recall
    result[:, 2] += f1
    for i in xrange(output_size):
        print "label" + str(i) + " precision: " + str(result[i, 0] / (idx_model + 1)),
        print "\trecall: " + str(result[i, 1] / (idx_model + 1)),
        print "\tf1: " + str(result[i, 2] / (idx_model + 1))
    print "------------------------------------"
    '''
    
    '''
    if error <= e:
        break
    '''

print "Finished. Time = " + str(time.time() - start)

