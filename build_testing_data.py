# -*- coding: utf-8 -*-
#pylint: skip-file
import sys
import os
import cPickle as pickle
import random
import string

LOW_FREQ_THRESHOLD = 2
LOW_FREQ_WORD = "lfw"
NUM_CROSS_VALIDATION = 1
TESTING_PROPORTION = 0.0
NUM_TRAINING_FILES = 20
NUM_TESTING_FILES = 5

def write_nyt_sent(sent, tag, path):
    words = sent.translate(None, string.punctuation).translate(None, string.digits).split()
    for w in words:
        if len(w) == 1:
            continue
        path.write(w + " " + tag + "\n")
    if tag == "1" or tag == "2":
        path.write("\n")

def build_nyt_training_data(f_tag1, f_tag2, f_entity, f_dst):
    num_tag1_sent = int(f_tag1.readline())
    num_tag2_sent = int(f_tag2.readline())
    epoch = num_tag1_sent if num_tag1_sent < num_tag2_sent else num_tag2_sent
    for i in xrange(epoch):
        l_tag1 = f_tag1.readline()
        write_nyt_sent(l_tag1, "1", f_dst)
        l_tag2 = f_tag2.readline()
        write_nyt_sent(l_tag2, "2", f_dst)
        for j in xrange(60):
            l_entity_tag = f_entity.readline().split("/")
            write_nyt_sent(l_entity_tag[0], l_entity_tag[1], f_dst)
    epoch = num_tag2_sent - num_tag1_sent if num_tag1_sent < num_tag2_sent else num_tag1_sent - num_tag2_sent
    f_remain = f_tag2 if num_tag1_sent < num_tag2_sent else f_tag1 
    tag_remain = "2" if num_tag1_sent < num_tag2_sent else "1" 
    for i in xrange(epoch):
        write_nyt_sent(f_remain.readline(), tag_remain, f_dst)

def build_nyt_testing_data(src_path, dst_path, output_size, split_char, has_tag, has_headline):
    data = []
    num_word = {}
    testing_w2i = {}

    with open(src_path, "r") as f_src:
        if has_headline:
            total_num_sents = int(f_src.readline())
        while 1:
            line = f_src.readline()
            if not line:
                break
            sent = []
            words = line.split()
            for pair in words:
                if split_char == None:
                    w = pair.translate(None, string.punctuation).translate(None, string.digits).lower()
                else:
                    w = pair.split(split_char)[0].translate(None, string.punctuation).translate(None, string.digits).lower()
                if w == "":
                    continue
                if w in num_word:
                    num_word[w] += 1
                else:
                    num_word[w] = 1
                sent.append(w)
            data.append(sent)

    dic_size = 0
    for w in num_word:
        if num_word[w] <= LOW_FREQ_THRESHOLD:
            continue
        testing_w2i[w] = dic_size
        dic_size += 1
    testing_w2i[LOW_FREQ_WORD] = dic_size
    pickle.dump(testing_w2i, open("./testing_w2i", "wb"))
    #num_separate = total_num_sents / NUM_TESTING_FILES

    split_file(data, dst_path, testing_w2i, NUM_TESTING_FILES, str(len(testing_w2i)), str(len(label)), has_tag)

    '''
    idx_offset = 0
    for idx_file in xrange(NUM_TESTING_FILES):
        with open(dst_path + str(idx_file), "w") as f_dst:
            idx_sent = 0
            num_sents = 0
            total_num_sents = num_separate if idx_file < NUM_TESTING_FILES - 1 else total_num_sents - idx_file * num_separate
            f_dst.write("                   \n") 
            while idx_sent < total_num_sents:
                if write_sent(f_dst, data[idx_offset], nyt_w2i):
                    num_sents += 1
                idx_sent += 1
                idx_offset += 1
            f_dst.seek(0, 0)
            f_dst.write(str(len(nyt_w2i)) + " " + str(output_size) + " " + str(int(num_sents)))
    '''

def split_file(data, dst_path, w2i, num_file, input_size, output_size, has_tag):
    num_total_sent = len(data)
    sizes = []
    for idx_file in xrange(num_file - 1):
        sizes.append(num_total_sent / num_file)
    sizes.append(num_total_sent / num_file + num_total_sent % num_file)

    write_func = write_sent_tag if has_tag else write_sent
    for idx_file in xrange(num_file):
        with open(dst_path + str(idx_file), "w") as f_dst:
            idx_start = sizes[0] * idx_file
            idx_end = idx_start + sizes[idx_file]
            f_dst.write("                   \n") 
            num_sent = 0
            for i in xrange(idx_start, idx_end):
                if write_func(f_dst, data[i], w2i):
                    num_sent += 1
            f_dst.seek(0, 0)
            f_dst.write(input_size + " " + output_size + " " + str(num_sent))

def write_sent(f_dst, sent, w2i):
    new_line = False
    for line in sent:
        if line in w2i:
            #line[0] = LOW_FREQ_WORD
            new_line = True
            f_dst.write(line+ "\n")
    if new_line:
        f_dst.write("\n")
    return new_line

def write_sent_tag(f_dst, sent, w2i):
    new_line = False
    for line in sent:
        if line[0] in w2i:
            #line[0] = LOW_FREQ_WORD
            #line[1] = "0"
            new_line = True
            f_dst.write(line[0] + " " + line[1] + "\n")
    if new_line:
        f_dst.write("\n")
    return new_line

def build_cross_validation_data(data, training_path, testing_path, w2i, output_size, num_file, testing_proportion):
    for idx_path in xrange(num_file):
        with open(training_path + "_" + str(idx_path), "w") as f_training:
            f_testing = None if testing_path == None else open(testing_path + "_" + str(idx_path), "w")
            total_testing_sent = int(len(data) * testing_proportion)
            total_training_sent = len(data) - num_testing_sent
            f_training.write("                   \n") 
            if testing_path != None:
                f_testing.write("                   \n") 
            random.shuffle(data)
            num_training_sent = 0
            num_testing_sent = 0
            i = 0
            if testing_path != None:
                while i < total_testing_sent:
                    if write_sent_tag(f_testing, data[i], w2i):
                        num_testing_sent += 1
                    i += 1
            while i < len(data):
                if write_sent_tag(f_training, data[i], w2i):
                    num_training_sent += 1
                i += 1
            f_training.seek(0, 0)
            f_training.write(str(len(w2i)) + " " + str(output_size) + " " + str(num_training_sent))
            if testing_path != None:
                f_testing.seek(0, 0)
                f_testing.write(str(len(w2i)) + " " + str(output_size) + " " + str(num_testing_sent))
                f_testing.close()

def build_data(num_file, testing_proportion, has_tag, binary_tag):
    if testing_proportion < 0 or testing_proportion > 1.0:
        print "wrong testing_proportion:" + str(testing_proportion)
        sys.exit(1)
    no_testing_file = True if abs(testing_proportion - 0.0) < 1e-8 else False

    root_dir = "/gds/zhwang/zhwang/data/cuhk/";
    src_dir = root_dir + "st/"
    training_path = "./training_data/training_data_"
    testing_path = None if no_testing_file else root_dir + "data/testing_data"
    i2w = {}
    w2i = {}
    num_word = {}
    label = []
    data = []

    if binary_tag:
        label = ["0", "1"]

    for ld in os.listdir(src_dir):
        src_path = os.path.join(src_dir, ld)
        with open(src_path, "r") as f_src:
            sent = []
            while 1:
                l = f_src.readline()
                if not l:
                    break
                l = l.strip('\n').lower()
                if l == "----":
                    continue
                if l == "":
                    data.append(sent)
                    sent = []
                    continue
                pair = l.split()
                pair[0] = pair[0].translate(None, string.punctuation).translate(None, string.digits)
                if pair[0] == "":
                    continue
                if binary_tag:
                    if pair[1] != "0":
                        pair[1] = "1"
                else:
                    if pair[1] not in label:
                        label.append(pair[1])
                if pair[0] not in num_word:
                    num_word[pair[0]] = 1
                else:
                    num_word[pair[0]] += 1
                sent.append(pair)
    dic_size = 0
    for w in num_word:
        if num_word[w] <= LOW_FREQ_THRESHOLD:
            continue
        w2i[w] = dic_size
        i2w[dic_size] = w
        dic_size += 1

    w2i[LOW_FREQ_WORD] = dic_size
    i2w[dic_size] = LOW_FREQ_WORD
    pickle.dump(i2w, open("./i2w", "wb"))
    pickle.dump(w2i, open("./w2i", "wb"))

    split_file(data, training_path, w2i, NUM_TRAINING_FILES, str(len(w2i)), str(len(label)), has_tag)
    #build_cross_validation_data(data, training_path, testing_path, w2i, len(label), num_file, testing_proportion)
    
    return label

root_dir = "/gds/zhwang/zhwang/data/cuhk/";
tag1_path = root_dir + "tag1"
tag2_path = root_dir + "tag2"
entity_path = "./entity2007"
dst_path = root_dir + "st/nyt"
'''
with open(tag1_path, "r") as f_tag1:
    with open(tag2_path, "r") as f_tag2:
        with open(entity_path, "r") as f_entity:
            with open(dst_path, "w") as f_dst:
                build_nyt_training_data(f_tag1, f_tag2, f_entity, f_dst)
'''

label = build_data(NUM_CROSS_VALIDATION, TESTING_PROPORTION, True, True)
#build_nyt_testing_data("./raw_nyt", "./nyt_testing/nyt_testing_", len(label), "/", True, True)


