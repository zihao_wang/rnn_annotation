# -*- coding: utf-8 -*-
#pylint: skip-file
import numpy as np
import theano
import theano.tensor as T
import cPickle as pickle
import sys
import os
import re
import string
import heapq
import operator.itemgetter

# set use gpu programatically
import theano.sandbox.cuda
def use_gpu(gpu_id):
    if gpu_id > -1:
        theano.sandbox.cuda.use("gpu" + str(gpu_id))

def floatX(X):
    return np.asarray(X, dtype=theano.config.floatX)

def init_weights(shape, name):
    return theano.shared(floatX(np.random.randn(*shape) * 0.1), name)

def init_gradws(shape, name):
    return theano.shared(floatX(np.zeros(shape)), name)

def init_bias(size, name):
    return theano.shared(floatX(np.zeros((size,))), name)

def save_model(f, model):
    ps = {}
    for p in model.params:
        ps[p.name] = p.get_value()
    pickle.dump(ps, open(f, "wb"))

def load_model(f, model):
    ps = pickle.load(open(f, "rb"))
    for p in model.params:
        p.set_value(ps[p.name])
    return model

def nth_largest(vec, nth):
    data = heapq.nlargest(nth, enumerate(vec), key = lambda x : x[1])
    nth_largest_val = min(data, key = itemgetter(1))[1]
    return filter(lambda e : e[1] == nth_largest_val, data)

def comp_sim(sent, others):
    intersects = (e & sent for e in others)
    unions = (e | sent for e in others)
    sims = map(lambda e1, e2 : float(len(e1)) / len(e2), intersects, union)
    
    if abs(sim1 - sim2) > PROB_DIFF:
        if sim1 > sim2:
            return (1, sim1)
        else:
            return (2, sim2)
    else:
        return (0, 0)


