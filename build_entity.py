# -*- coding: utf-8 -*-
#pylint: skip-file
import sys
import os

def read_data(path_src, path_dst):
    useful_tag = ["PERSON", "LOCATION", "DATE", "ORGANIZATION", "DURATION", "TIME"]
    who_tag = ["PERSON", "ORGANIZATION"]
    where_tag = ["LOCATION"]
    when_tag = ["DATE", "DURATION", "TIME"]
    punc = ".,!?'-\":;()"
    num_who = 0
    num_where = 0
    num_when = 0
    with open(path_dst, "w") as f_dst:
        with open(path_src, "r") as f_src:
            for line in f_src:
                words = line.split()
                idx_now = 0
                while idx_now < len(words):
                    pair = words[idx_now].split("/")
                    tag = pair[1]
                    if len(pair) != 2 or pair[0] in punc:
                        idx_now += 1
                        continue
                    if tag in useful_tag:
                        phrase_words = []
                        if pair[1] in who_tag:
                            tag_num = "3"
                            num_who += 1
                        elif pair[1] in when_tag:
                            tag_num = "4"
                            num_when += 1
                        else:
                            tag_num = "5"
                            num_where += 1
                        phrase_words.append(pair[0].lower())
                        idx_phrase = idx_now + 1

                        while 1:
                            pair = words[idx_phrase].split("/")
                            if len(pair) != 2 or pair[0] in punc:
                                break
                            if pair[1] == tag:
                                phrase_words.append(pair[0].lower())
                                idx_phrase += 1
                            else:
                                break
                                
                        phrase = ""
                        for word in phrase_words:
                            phrase += word
                            phrase += " "

                        f_dst.write(phrase[:-1] + "/" + tag_num + "\n") 
                        idx_now = idx_phrase

                    idx_now += 1
    print "#who:", num_who, "#when:", num_when, "#where:", num_where

read_data("/misc/projdata12/info_fil/zhwang/data/NYT/entity2007", "./entity2007")
