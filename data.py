# -*- coding: utf-8 -*-
#pylint: skip-file
import sys
import os
import numpy as np
import theano
import theano.tensor as T

np.set_printoptions(threshold = np.nan)
curr_path = os.path.dirname(os.path.abspath(os.path.expanduser(__file__)))
v_dim_x = 200
LOW_FREQ_WORD = "lfw"

class raw_data:
    def __init__(self):
        self.word = []
        self.anno = []

class batch_data:
    def __init__(self, local_topic, trans, using_w2v, max_sent_len, input_size, output_size, without_tag):
        self.local_batch_size = len(local_topic)
        self.x = np.zeros((max_sent_len, input_size * self.local_batch_size), dtype = theano.config.floatX)
        if without_tag:
            self.y = None
        else:
            self.y = np.zeros((max_sent_len, output_size * self.local_batch_size), dtype = theano.config.floatX)
        self.mask = np.zeros((max_sent_len, self.local_batch_size), dtype = theano.config.floatX)
        self.num_word = []
        self.sentence = []

        w2v_hit = 0
        w2v_total = 0
        for idx_sent in xrange(self.local_batch_size):
            each_sent = local_topic[idx_sent]
            x_offset = idx_sent * input_size
            y_offset = idx_sent * output_size
            sent = ""
            for idx_word in xrange(len(each_sent.word)):
                if using_w2v:
                    if each_sent.word[idx_word] in trans.vocab:
                        v = trans[each_sent.word[idx_word]]
                        w2v_hit += 1
                    else:
                        v = np.random.uniform(-0.25, 0.25, v_dim_x)
                    self.x[idx_word, x_offset : x_offset + input_size] = v
                    w2v_total += 1
                else:
                    if each_sent.word[idx_word] in trans:
                        w = each_sent.word[idx_word]
                    else:
                        w = LOW_FREQ_WORD
                    self.x[idx_word, x_offset + trans[w]] = 1
                if not without_tag:
                    self.y[idx_word, y_offset + int(each_sent.anno[idx_word])] = 1
                self.mask[idx_word, idx_sent] = 1
                sent += each_sent.word[idx_word]
                if idx_word < len(each_sent.word) - 1:
                    sent += " "
            self.num_word.append(len(each_sent.word))
            self.sentence.append(sent)
        #if using_w2v:
        #    print "w2v hit percentage:", float(w2v_hit) / w2v_total

def read_data(f_path, trans, using_w2v, batch_size, delete_nontagged, without_tag, has_headline, output_size = -1):
    MIN_LENGTH = 5
    MAX_LENGTH = 50
    topic = []

    f = open(f_path, "r")
    tagged = False
    each_sent = raw_data()
    max_sent_len = 0

    input_size = v_dim_x if using_w2v else len(trans)
    if has_headline:
        line = f.readline()
        sizes = line.split()
        output_size = int(sizes[1])

    while 1:
        line = f.readline()
        if not line:
            break
        line = line.strip('\n').lower()
        if line == "":
            if len(each_sent.word) < MIN_LENGTH or len(each_sent.word) > MAX_LENGTH:
                tagged = False
                each_sent = raw_data()
                continue

            if not (delete_nontagged and not tagged):
                if len(each_sent.word) > max_sent_len:
                    max_sent_len = len(each_sent.word)
                topic.append(each_sent)
            tagged = False
            each_sent = raw_data()
            continue

        pair = line.split()
        each_sent.word.append(pair[0])
        if not without_tag:
            each_sent.anno.append(pair[1])
            if int(pair[1]) != 0:
                tagged = True
    f.close()

    data_xy = []
    num_sentence = len(topic)
    num_batch = num_sentence / batch_size if num_sentence % batch_size == 0 else num_sentence / batch_size + 1
    for i in xrange(num_batch - 1):
        each_batch = batch_data(topic[i * batch_size : (i + 1) * batch_size], trans, using_w2v, max_sent_len, input_size, output_size, without_tag)
        data_xy.append(each_batch)
    last_batch = batch_data(topic[(num_batch - 1) * batch_size : ], trans, using_w2v, max_sent_len, input_size, output_size, without_tag)
    data_xy.append(last_batch)

    return data_xy, num_sentence

def read_size(path, using_w2v, trans):
    with open(path, "r") as f_src:
        line = f_src.readline()
        sizes = line.split()
        input_size = v_dim_x if using_w2v else len(trans)
        output_size = int(sizes[1])

    return input_size, output_size
