# -*- coding: utf-8 -*-
#pylint: skip-file
import sys
import os
import re
import string
try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET
from xml.dom import minidom

CATEGORY = {
        "1" : ["WHAT", "WHEN", "WHERE", "WHY", "WHO_AFFECTED", "DAMAGES", "COUNTERMEASURES"],
        "2" : ["WHAT", "WHEN", "WHERE", "PERPETRATORS", "WHY", "WHO_AFFECTED", "DAMAGES", "COUNTERMEASURES"],
        "3" : ["PRODUCT_NAME", "WHO_INVENTED", "WHAT", "WHY", "NEW_FEATURES", "EFFECTS", "COUNTERMEASURES"],
        "4" : ["WHAT", "WHO_AFFECTED", "HOW", "WHY", "COUNTERMEASURES"],
        "5" : ["WHAT", "IMPORTANCE", "THREATS", "COUNTERMEASURES"],
        "6" : ["WHO", "WHO_INV", "WHY", "CHARGES", "PLEAD", "SENTENCE"]
        }

def prettify_xml(elem):
    raw_str = ET.tostring(elem, "utf-8")
    reparsed = minidom.parseString(raw_str)
    return reparsed.toprettyxml(indent = "    ")

def topic_generator(root):
    listdir = os.listdir(root)
    for ld in listdir:
        topic_path = os.path.join(root, ld)
        info = ld.split("_")
        topic_info_path = info[1].lower() + ".xml"
        if topic_info_path not in listdir:
            topic_info_path = info[1] + ".xml"
        
        doc = ET.parse(topic_path + "/" + topic_info_path)
        root = doc.getroot()
        aspect_path = topic_path + "/aspect_facet/"
        yield aspect_path, root.attrib
    yield None, None

def xml_srcfile_generator(root):
    prefix = "aspect_facet_Raw-Topic-"
    postfix = "score.xml"
    len_prefix = len(prefix)
    len_postfix = len(postfix)
    for ld in os.listdir(root):
        len_ld = len(ld)
        if ld.find(prefix, 0, len_prefix) == 0 and ld.rfind(postfix, len_ld - len_postfix, len_ld) == len_ld - len_postfix:
            yield os.path.join(root, ld)
    yield None

def collect_summary(path, summary):
    raw_xml = ""
    with open(path, "r") as f_src:
        for line in f_src:
            raw_xml += line.strip("\n")
    doc = ET.parse(path)
    root = doc.getroot()
    for ctgr in root:
        for facet in ctgr:
            for mention in facet.iter("mention"):
                summary[ctgr.tag].append(mention.attrib["doc_id"] + "~~" + mention.text)

def write_new_xml(path, summary, topic_info):
    root = ET.Element("facet_summary", {"id" : topic_info["id"]})
    for k in summary:
        tag = ET.SubElement(root, k)
        facet = ET.SubElement(tag, "facet")
        for e in summary[k]:
            summ = ET.SubElement(facet, "summary")
            summ.text = e
    print prettify_xml(root)

def combine_cuhk_aspect_xml():
    cuhk_root = "/misc/projdata12/info_fil/zhwang/data/cuhk/"
    tg = topic_generator(cuhk_root + "raw-topics/")
    topic, topic_info = tg.next()
    while topic != None:
        xg = xml_srcfile_generator(topic)
        xml_file = xg.next()
        topic_ctgr = CATEGORY[topic_info["category"]]
        summary = {}
        for e in topic_ctgr:
            summary[e] = []
    
        while xml_file != None:
            #print xml_file, topic_info
            collect_summary(xml_file, summary)
            xml_file = xg.next()
        write_new_xml(cuhk_root + "new_xml/" + topic_info["id"], summary, topic_info)
        sys.exit(0)
        topic, topic_info = tg.next()

combine_cuhk_aspect_xml()
