#pylint: skip-file
import numpy as np
import theano
import theano.tensor as T
import sys
from utils_pg import *

from softmax import *
from gru import *
from lstm import *
from logistic import *
from updates import *

class RNN(object):
    def __init__(self, in_size, out_size, hidden_size,
                 cell = "gru", optimizer = "rmsprop", p = 0.5):
        self.X = T.matrix("X")
        self.in_size = in_size
        self.out_size = out_size
        self.hidden_size = hidden_size
        self.cell = cell
        self.drop_rate = p
        self.is_train = T.iscalar('is_train') # for dropout
        self.batch_size = T.iscalar('batch_size') # for mini-batch training
        self.mask = T.matrix("mask")
        self.optimizer = optimizer
        self.define_layers()
        self.define_train_test_funcs()
        
    def define_layers(self):
        self.layers = []
        self.params = []
        rng = np.random.RandomState(1234)
        # hidden layers
        for i in xrange(len(self.hidden_size)):
            if i == 0:
                layer_input = self.X
                shape = (self.in_size, self.hidden_size[0]) 
            else:
                layer_input = self.layers[i - 1].activation
                shape = (self.layers[i - 1].out_size, self.hidden_size[i]) 

            if self.cell == "gru":
                hidden_layer = GRULayer(rng, str(i), shape, layer_input,
                                        self.mask, self.is_train, self.batch_size, self.drop_rate)
            elif self.cell == "lstm":
                hidden_layer = LSTMLayer(rng, str(i), shape, layer_input,
                                         self.mask, self.is_train, self.batch_size, self.drop_rate)
            
            self.layers.append(hidden_layer)
            self.params += hidden_layer.params

        self.output_layer = SoftmaxLayer((hidden_layer.out_size, self.out_size), 
                                    hidden_layer.activation, self.batch_size)
        self.params += self.output_layer.params
   
    # https://github.com/fchollet/keras/pull/9/files
        self.epsilon = 1.0e-15
    def categorical_crossentropy(self, y_pred, y_true):
        y_pred = T.clip(y_pred, self.epsilon, 1.0 - self.epsilon)
        return T.nnet.categorical_crossentropy(y_pred, y_true).mean()

    def define_train_test_funcs(self):
        activation = self.output_layer.activation
        self.Y = T.matrix("Y")
        lr = T.scalar("lr")
        #'''
        cost = self.categorical_crossentropy(activation, self.Y)
        gparams = []
        for param in self.params:
            #gparam = T.grad(cost, param)
            gparam = T.clip(T.grad(cost, param), -10, 10)
            gparams.append(gparam)

        # eval(): string to function
        optimizer = eval(self.optimizer)
        updates = optimizer(self.params, gparams, lr)

        #updates = sgd(self.params, gparams, lr)
        #updates = momentum(self.params, gparams, lr)
        #updates = rmsprop(self.params, gparams, lr)
        #updates = adagrad(self.params, gparams, lr)
        #updates = dadelta(self.params, gparams, lr)
        #updates = adam(self.params, gparams, lr)
        
        self.train = theano.function(inputs = [self.X, self.mask, self.Y, lr, self.batch_size],
                                               givens = {self.is_train : np.cast['int32'](1)},
                                               outputs = [cost],
                                               updates = updates,
                                               on_unused_input='ignore')
        self.predict = theano.function(inputs = [self.X, self.mask, self.batch_size],
                                                 givens = {self.is_train : np.cast['int32'](0)},
                                                 outputs = [activation])
                                                 #outputs = [activation, a0, a1])
  
        #theano.printing.pydotprint(self.train, outfile="./model/train.png", var_with_name_simple=True) 
        #'''

class BdRNN(object):
    def __init__(self, in_size, out_size, hidden_size,
                 cell = "gru", optimizer = "rmsprop", p = 0.5):
        self.X = T.matrix("X")
        self.in_size = in_size
        self.out_size = out_size
        self.hidden_size = hidden_size
        self.bd_hidden_size = hidden_size[len(hidden_size) - 1]
        self.cell = cell
        self.drop_rate = p
        self.is_train = T.iscalar('is_train') # for dropout
        self.batch_size = T.iscalar('batch_size') # for mini-batch training
        self.mask = T.matrix("mask")
        self.optimizer = optimizer
        self.define_layers()
        self.define_train_test_funcs()
        
    def define_layers(self):
        self.flayers = []
        self.blayers = []
        self.params = []
        rng = np.random.RandomState(1234)
        # hidden layers
        for i in xrange(len(self.hidden_size)):
            if i == 0:
                flayer_input = self.X
                blayer_input = self.X[::-1]
                fshape = (self.in_size, self.hidden_size[0]) 
                bshape = (self.in_size, self.hidden_size[0]) 
            else:
                flayer_input = self.flayers[i - 1].activation
                blayer_input = self.blayers[i - 1].activation
                fshape = (self.flayers[i - 1].out_size, self.hidden_size[i]) 
                bshape = (self.blayers[i - 1].out_size, self.hidden_size[i]) 

            if self.cell == "gru":
                fhidden_layer = GRULayer(rng, str(i), fshape, flayer_input,
                                        self.mask, self.is_train, self.batch_size, self.drop_rate)
                bhidden_layer = GRULayer(rng, str(i), bshape, blayer_input,
                        self.mask[::-1], self.is_train, self.batch_size, self.drop_rate)
            elif self.cell == "lstm":
                fhidden_layer = LSTMLayer(rng, str(i), fshape, flayer_input,
                                         self.mask, self.is_train, self.batch_size, self.drop_rate)
                bhidden_layer = LSTMLayer(rng, str(i), bshape, blayer_input,
                        self.mask[::-1], self.is_train, self.batch_size, self.drop_rate)
            
            self.flayers.append(fhidden_layer)
            self.blayers.append(bhidden_layer)
            self.params += fhidden_layer.params
            self.params += bhidden_layer.params

        self.bd_fW = init_weights((self.bd_hidden_size, self.bd_hidden_size), "bd_fW")
        self.bd_bW = init_weights((self.bd_hidden_size, self.bd_hidden_size), "bd_fW")
        self.bd_b = init_bias(self.bd_hidden_size, "bd_b")
        self.params += [self.bd_fW, self.bd_bW, self.bd_b]

        fh = fhidden_layer.activation
        fh = T.reshape(fh, (self.batch_size * self.X.shape[0], self.bd_hidden_size))
        bh = T.reshape(bhidden_layer.activation, (self.batch_size * self.X.shape[0], self.bd_hidden_size))
        bd_output = T.tanh(T.dot(fh, self.bd_fW) + T.dot(bh, self.bd_bW) + self.bd_b)
        bd_output = T.reshape(bd_output, (self.X.shape[0], self.bd_hidden_size * self.batch_size))
        '''
        def bd_active(fh, bh, m, fW, bW, b):
            fh = fh.reshape((self.batch_size, self.bd_hidden_size))
            bh = bh.reshape((self.batch_size, self.bd_hidden_size))
            bd_h = T.tanh(T.dot(fh, fW) + T.dot(bh, bW) + b)
            bd_h *= m[:, None]
            bd_h = bd_h.reshape((1, self.batch_size * self.bd_hidden_size))
            return bd_h
        bd_scan_output, updates = theano.scan(bd_active, sequences = [fhidden_layer.activation, bhidden_layer.activation, self.mask],
                non_sequences = [self.bd_fW, self.bd_bW, self.bd_b])
        bd_output = bd_scan_output.reshape((self.X.shape[0], self.batch_size * self.bd_hidden_size))
        '''
        self.output_layer = SoftmaxLayer((self.bd_hidden_size, self.out_size), 
                                    bd_output, self.batch_size)
        self.params += self.output_layer.params
   
    # https://github.com/fchollet/keras/pull/9/files
        self.epsilon = 1.0e-15
    def categorical_crossentropy(self, y_pred, y_true):
        y_pred = T.clip(y_pred, self.epsilon, 1.0 - self.epsilon)
        return T.nnet.categorical_crossentropy(y_pred, y_true).mean()

    def define_train_test_funcs(self):
        activation = self.output_layer.activation
        self.Y = T.matrix("Y")
        lr = T.scalar("lr")
        #'''
        cost = self.categorical_crossentropy(activation, self.Y)
        gparams = []
        for param in self.params:
            #gparam = T.grad(cost, param)
            gparam = T.clip(T.grad(cost, param), -10, 10)
            gparams.append(gparam)

        # eval(): string to function
        optimizer = eval(self.optimizer)
        updates = optimizer(self.params, gparams, lr)

        #updates = sgd(self.params, gparams, lr)
        #updates = momentum(self.params, gparams, lr)
        #updates = rmsprop(self.params, gparams, lr)
        #updates = adagrad(self.params, gparams, lr)
        #updates = dadelta(self.params, gparams, lr)
        #updates = adam(self.params, gparams, lr)
        
        self.train = theano.function(inputs = [self.X, self.mask, self.Y, lr, self.batch_size],
                                               givens = {self.is_train : np.cast['int32'](1)},
                                               outputs = [cost],
                                               updates = updates,
                                               on_unused_input='ignore')
        self.predict = theano.function(inputs = [self.X, self.mask, self.batch_size],
                                                 givens = {self.is_train : np.cast['int32'](0)},
                                                 outputs = [activation])
                                                 #outputs = [activation, a0, a1])
  
        #theano.printing.pydotprint(self.train, outfile="./model/train.png", var_with_name_simple=True) 
        #'''
