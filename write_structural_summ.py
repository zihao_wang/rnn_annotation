# -*- coding: utf-8 -*-
#pylint: skip-file
import os
import sys

def has_entity_tag(tag):
    return True if tag == "3" or tag == "4" or tag == "5" else False

def has_long_tag(tag):
    return True if tag == "1" or tag == "2" else False

def has_tag(tag):
    return True if has_entity_tag(tag) or has_long_tag(tag) else False

def count_tag(tags, idx_now, width):
    num_tag1 = 0
    num_tag2 = 0
    i = idx_now - width
    while i <= idx_now + width:
        if tags[i] == "1":
            num_tag1 += 1
        elif tags[i] == "2":
            num_tag2 += 1
        i += 1

    return num_tag1, num_tag2

def transform_entity_tag(tags):
    if has_entity_tag(tags[0]) and has_long_tag(tags[1]):
        tags[0] = tags[1]
    i = 1
    while i < len(tags) - 1:
        if has_entity_tag(tags[i]):
            if tags[i - 1] == "2" and tags[i + 1] == "2":
                tags[i] = "2"
                i += 1
            elif tags[i - 1] == "1" and (tags[i + 1] == "1" or tags[i + 1] == "2"):
                tags[i] = "1"
                i += 1
            elif tags[i - 1] == "2" and tags[i + 1] == "1":
                tags[i] = "1"
                i += 1
        i += 1
    i = len(tags) - 1
    if has_entity_tag(tags[i]) and has_long_tag(tags[i - 1]):
        tags[i] = tags[i - 1]

def fill_vacancy(tags):
    width = 2
    i = width
    while i < len(tags) - width:
        if not has_tag(tags[i]):
            num_tag1, num_tag2 = count_tag(tags, i, width)
            if num_tag1 + num_tag2 == 2 * width:
                tags[i] = ("1" if num_tag1 >= num_tag2 else "2")
                i += width
        i += 1

def binary_fill_vacancy(tags):
    width = 2
    i = width
    while i < len(tags) - width:
        if tags[i] != "1":
            num_tag1 = count_tag(tags, i, width)[0]
            if num_tag1 == 2 * width:
                tags[i] = "1"
                i += width
        i += 1


def choose_summ_in_sent(summ, words, tags, probs):
    has_summ = False

    #transform_entity_tag(tags)
    fill_vacancy(tags)

    i = 0
    num_tags = [0, 0, 0, 0, 0]
    while i < len(words):
        if has_tag(tags[i]):
            has_summ = True
            num_tags[int(tags[i]) - 1] += 1
        i += 1

    more_tag1 = True if num_tags[0] >= num_tags[1] else False

    if more_tag1:
        for i in xrange(len(tags)):
            if tags[i] == "2":
                tags[i] = "1"
    else:
        for i in xrange(len(tags)):
            if tags[i] == "1":
                tags[i] = "2"

    if sum(num_tags) > 3 and float(sum(num_tags)) / len(words) > 0.8:
        for i in xrange(len(tags)):
            if tags[i] == "0":
                tags[i] = "1" if more_tag1 else "2"

    if num_tags[0] + num_tags[1] <= 3 and num_tags[2] + num_tags[3] + num_tags[4] == 0:
        for i in xrange(len(tags)):
            tags[i] = "0"
        has_summ = False

    if not has_summ:
        pass
    else:
        i = 0
        while i < len(tags):
            if has_tag(tags[i]):
                tmp = words[i]
                num_words = 1
                tag = tags[i]
                j = i + 1
                while j < len(tags):
                    if tags[j] != tag:
                        break
                    tmp += " " + words[j]
                    j += 1
                    num_words += 1
                if num_words > 4:
                    summ[int(tag) - 1].append(tmp)
                i = j
            else:
                i += 1
    '''
    if len(summ[0]) > 0:
        print summ
        sys.exit(0)
    '''
def choose_binary_summ_in_sent(summ, words, tags, probs):
    has_summ = False

    binary_fill_vacancy(tags)

    i = 0
    num_tags = 0 
    while i < len(words):
        if tags[i] == "1":
            has_summ = True
            num_tags += 1
        i += 1

    if num_tags > 3 and float(num_tags) / len(words) > 0.8:
        for i in xrange(len(tags)):
            if tags[i] == "0":
                tags[i] = "1"

    if num_tags <= 3:
        for i in xrange(len(tags)):
            tags[i] = "0"
        has_summ = False

    if not has_summ:
        pass
    else:
        i = 0
        while i < len(tags):
            if tags[i] == "1":
                tmp = words[i]
                num_words = 1
                tag = tags[i]
                j = i + 1
                while j < len(tags):
                    if tags[j] == "0":
                        break
                    tmp += " " + words[j]
                    j += 1
                    num_words += 1
                if num_words > 4:
                    summ[int(tag) - 1].append(tmp)
                i = j
            else:
                i += 1
    '''
    if len(summ[0]) > 0:
        print summ
        sys.exit(0)
    '''


def write_each_news(summ, f_dst, info):
    f_dst.write(info)
    for e in summ:
        f_dst.write(e)
        f_dst.write("\n")
    f_dst.write("\n")


def write_news_summ(summ, f_dst, idx_news):
    f_dst.write("news " + str(idx_news + 1) + "\n")
    write_each_news(summ[0], f_dst, "what & why:\n")
    write_each_news(summ[1], f_dst, "result:\n")
    write_each_news(summ[2], f_dst, "who:\n")
    write_each_news(summ[3], f_dst, "when:\n")
    write_each_news(summ[4], f_dst, "where:\n")
    f_dst.write("\n-------------------------------------------")

def write_binary_news_summ(summ, f_dst, idx_news):
    f_dst.write("news " + str(idx_news + 1) + "\n")
    write_each_news(summ[0], f_dst, "summary:\n")
    f_dst.write("\n-------------------------------------------")

def write_summ(output_size):
    src_dir = "/misc/projdata12/info_fil/zhwang/workspace/rnn_text_annotation/tac/rnn_prediction/"
    dst_dir = "/misc/projdata12/info_fil/zhwang/workspace/rnn_text_annotation/tac/structural_summ/"
    num_news_dir = "/misc/projdata12/info_fil/zhwang/workspace/rnn_text_annotation/tac/num_news/"

    num_news = {}
    for ld in os.listdir(num_news_dir):
        num_path = os.path.join(num_news_dir, ld)
        with open(num_path, "r") as f_num:
            num = []
            for line in f_num:
                num.append(int(line))
            num_news[ld] = num

    for ld in os.listdir(src_dir):
        cur_num = num_news[ld]
        src_path = os.path.join(src_dir, ld)
        dst_path = dst_dir + ld
        with open(src_path, "r") as f_src:
            with open(dst_path, "w") as f_dst:
                words = []
                tags = []
                probs = []
                sent = []
                summ = []
                for i in xrange(output_size - 1):
                    summ.append([])
                idx_sent = 0
                idx_news = 0
                num_total_sent = 0
                for line in f_src:
                    if line == "\n":
                        for w in sent:
                            tmp = w.split("\\")
                            words.append(tmp[0])
                            tags.append(tmp[1])
                            probs.append(tmp[2])
                        if output_size == 2:
                            choose_binary_summ_in_sent(summ, words, tags, probs)
                        else:
                            choose_summ_in_sent(summ, words, tags, probs)
                        words = []
                        tags = []
                        probs = []
                        sent = []
                        idx_sent += 1
                        num_total_sent += 1
                        if idx_sent >= cur_num[idx_news]:
                            if output_size == 2:
                                write_binary_news_summ(summ, f_dst, idx_news)
                            else:
                                write_news_summ(summ, f_dst, idx_news)
                            summ = []
                            for i in xrange(output_size - 1):
                                summ.append([])
                            idx_sent = 0
                            idx_news += 1
                        continue
                    sent.append(line)

write_summ(2)
